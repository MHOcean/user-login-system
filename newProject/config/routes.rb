Rails.application.routes.draw do
  get 'blogs/index'
  get 'blogs/show'
  get 'blogs/new'
  get 'blogs/create'
  get 'blogs/edit'
  get 'blogs/update'
  get 'blogs/destroy'
  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  root 'blogs#index'
  resources :users
  resources :blogs
  resources :sessions, only: %i[new create destroy]
  get 'users/create'
  get 'users/new'
  get 'users/edit'
  get 'users/show'
  get 'users/update'
  get 'users/destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
