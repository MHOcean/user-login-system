class Blog < ApplicationRecord
  validates :title, presence: true, on: :create
  validates :content, presence: true, on: :create
  belongs_to :user
end
