class User < ApplicationRecord
  validates :name, presence: true, on: :create
  validates :email, presence: true, on: :create
  validates :password, presence: true, on: :create
  validates :password_confirmation, presence: true, on: :create
  has_secure_password
  validates_uniqueness_of :email
  has_many :blogs
end
