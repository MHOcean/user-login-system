# frozen_string_literal: true

class BlogsController < ApplicationController

  before_action :find_blog_id, only: %i[show edit destroy update]

  def index
    @blog = Blog.all
    render :index
  end

  def show
    render :show
  end

  def new
    @blog = Blog.new
    render :new
  end

  def create
    @blog = Blog.new(blog_params)
    @blog.user = current_user
    if @blog.save!
      redirect_to blogs_path
    else
      render :new
    end
  end

  def edit; end

  def update
    if @blog.update_attributes(blog_params)
      redirect_to blog_path(@blog)
    else
      render :edit
    end
  end

  def destroy
    @blog.destroy
    redirect_to blogs_path
  end

  private

  def blog_params
    params.require(:blog).permit(:title, :content)
  end

  def find_blog_id
    @blog = Blog.find(params[:id])
  end
end
