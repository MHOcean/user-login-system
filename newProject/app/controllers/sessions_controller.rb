# frozen_string_literal: true

class SessionsController < ApplicationController
  def new; end

  def create
    user = User.find_by_email(params[:email])
    if user&.authenticate(params[:password])

      session[:user_id] = user.id
      redirect_to blogs_path
    else
      render :new
    end
  end

  def destroy
    reset_session
    redirect_to blogs_path, notice: 'Logged out!'
  end
end
