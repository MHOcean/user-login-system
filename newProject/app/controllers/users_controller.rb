# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :find_user_id, only: %i[show edit destroy update]
  # def index
  #   @user = User.all
  #   render :index
  # end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to blogs_path
    else
      render :new
    end
  end

  def new
    @user = User.new
    render :new
  end

  def show
    render :show
  end

  def edit; end

  def update
    if @user.update_attributes!(user_params)
      redirect_to @user
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to users_path
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :age, :password,
                                 :password_confirmation, :bio)
  end

  def find_user_id
    @user = User.find(params[:id])
  end
end
